" Must be VIMproved lol
set nocompatible
filetype off

" Make runtime path include vundle
set rtp+=~/.vim/bundle/Vundle.vim

" Initialize vundle
call vundle#begin()

" Make vundle deal w/ vundle (required)
Plugin 'VundleVim/Vundle.vim'

" Git wrapper
Plugin 'tpope/vim-fugitive'

" More git
Plugin 'tpope/vim-git'

" Help w/ closing tags
Plugin 'Raimondi/delimitMate'

" Markdown syntax
Plugin 'gmarik/vim-markdown'

" Fuzzy file/buffer/etc
Plugin 'ctrlpvim/ctrlp.vim'

" js support
Plugin 'pangloss/vim-javascript'
Plugin 'nathanaelkane/vim-indent-guides'

" Java helpers
Plugin 'SirVer/ultisnips'
Plugin 'Yggdroot/indentLine'
Plugin 'Raimondi/delimitMate'
Plugin 'ervandew/supertab'

" LaTex support
Plugin 'LaTeX-Box-Team/LaTeX-Box'

" Syntastic lol
Plugin 'scrooloose/syntastic'

" Nerdtree :)
Plugin 'scrooloose/nerdtree'

" For quoting, parenthesizing
Plugin 'tpope/vim-surround'

" Status/tabline
Plugin 'bling/vim-airline'

" Autocompletion
" Plugin 'Valloric/YouCompleteMe'

" Tern
Plugin 'marijnh/tern_for_vim'

" Solarized ;)
Plugin 'altercation/vim-colors-solarized'

" Refer to all plugins before this line
call vundle#end()
filetype indent plugin on
syntax enable
" Always display the statusline in all windows
set laststatus=2 

" Always display the tabline, even if there is only one tab
set showtabline=2 

" Use 2 spaces for indentation
set expandtab
set shiftwidth=2
set softtabstop=2

" vertical line indentation 
let g:indentLine_color_term = 239 
let g:indentLine_color_gui = '#09AA08'
let g:indentLine_char = '│'

" Hide the default mode text (e.g. -- INSERT -- below the statusline)"
set noshowmode 

" Appearance stuff
" colorscheme koehler
set background=dark
set number

noremap <CR> G
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
imap <C-l> <CR><Esc>O
set cursorline
set colorcolumn=80

let g:syntastic_check_on_open=1
