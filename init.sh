#!/bin/bash

# Installations
sudo apt-get install -y apache2 php5 mysql-server vim nmap netcat git zsh

# Set runtime configuration for vim
curl -L -O https://bitbucket.org/michaelnetbiz/sys/raw/dfaf4bddef6240b49a000b9f276a0f398803670c/config/.vimrc

# Up & ug
sudo apt-get update
sudo apt-get upgrade

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
